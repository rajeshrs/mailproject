import os
from configparser import ConfigParser


def get_config(section):
    parser = ConfigParser()
    parser.optionxform = str
    config_file = os.path.join(os.path.dirname(__file__), 'config.ini')
    parser.read(config_file)
    section_options = parser.options(section)
    section_config = {key: parser.get(section, key) for key in section_options}
    return section_config
