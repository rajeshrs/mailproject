Python version 3.7

### Steps to Setup the Project
 
- Once you downloaded the repo, create a new virtual environment.

- Make sure you installed the necessary python packages for this project

  ```pip install -r requirements.txt```

- Create a `config.ini` file, provide any of your gmail user's email address under *gmail* section for testing(
refer the file `config.ini.template)`

- And provide your mysql db *host*, *username* and *password* under the *mysql* section in config.ini. Provide any name for 
database. Scripts will try to use the database if exists else it will create a database with the name provided in the config

- Now we are ready to test the project

- Run the `fetch.py` to fetch the mails from your gmail inbox and it will redirect to browser at localhost:8000.

- With the user email you provided in the config, you need to give `consent` to the oauth application to access your private mail's data.

- Once done, it will become authorized and fetch the mails from your inbox and store it in the mysql db in a
 table `mails`.(Note: It will take some time to fetch the emails from your inbox if you have massive number of mails/massive text size)
 Also, you can run `fetch.py` multiple times, it will not create duplicate records. 

- Then you can run `process.py` to process the mails from the db with some set of conditions and to do actions based on the rules in the
 `rules.json`.
 
### Project Detailed structure:
 
 #### MailProject  
    --- config.ini (stores the mysql config)
    --- credentials.json (Gmail oauth application credentials)
    --- gmail_api.py (gmail api rest operations)  
    --- db.py (mysql db operations)  
    --- util.py (utility methods to parse the email data and datetime conversion)  
    --- fetch.py (fetch the emails from gmail inbox and store it in db)   
    --- process.py (process the emails from the db table and do actions based on the rules)  
    --- rules.json (rules with an overall predicate and set of conditions and actions)
    --- token.pickle (stores the user's access and refresh tokens, and it will
            be created automatically when the authorization flow completes for the first
            time)

### Third party Libraries Used:

- *google-api-python-client* - for gmail api authorization and rest operations

- *google-auth-httplib2 and google-auth-oauthlib* - also used for gmail api authorization

- *mysql-connector-python* - for mysql db operations

- *beautifulsoup4* - to parse and convert html to text

- *pytz* - for the datetime conversion