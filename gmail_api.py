import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from util import parse_header, parse_body
from __init__ import get_config

# If modifying these scopes, delete the file token.pickle.
SCOPES = [
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/gmail.modify'
]


class GmailAPI(object):

    def __init__(self):
        self.section = 'gmail'
        self.config = get_config(self.section)
        self.host = self.config.get('HOSTNAME')
        self.service = self.authorize()
        self.user_id = self.config.get('USER_EMAIL')

    @staticmethod
    def authorize():
        """
        Authorize application to access user's data

        Returns:
            authorized gmail API service instance
        """
        creds = None

        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=8000)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('gmail', 'v1', credentials=creds)
        return service

    def fetch_mails(self):
        """
        Get a list of all mails in the user's mailbox

        Returns:
            list of mails with specific fields in json

        """
        mail_response = self.service.users().messages().list(userId=self.user_id).execute()
        results = mail_response.get('messages', [])
        mails = []
        for result in results:
            response = self.service.users().messages().get(userId='me', id=result['id'], format='full').execute()
            label_ids = response.get('labelIds')
            payload = response.get('payload')
            headers = payload.get('headers')
            mail_subject, mail_from, received_time = parse_header(headers)
            body_message = parse_body(payload)
            mail = {
                'id': result['id'],
                'mail_from': mail_from,
                'mail_subject': mail_subject,
                'message': body_message,
                'label_ids': str(label_ids),
                'received_time': received_time
            }
            mails.append(mail)

        return mails

    def list_labels(self):
        """
        Get a list of all labels in the user's mailbox.

        Returns:
            list of all labels in the user's mailbox
        """
        try:
            response = self.service.users().labels().list(userId=self.user_id).execute()
            labels = response['labels']
            return labels
        except Exception as err:
            raise err

    def modify(self, message_id, msg_labels):
        """
        Marks the mail as read in the user's mailbox
        Args:
            message_id: Id of the mail
            msg_labels: payload with the list of labels that are going to modify
        Returns:
            Modified mail, containing updated labelIds, id and threadId.
        """
        try:
            message = self.service.users().messages().modify(userId=self.user_id,
                                                             id=message_id, body=msg_labels).execute()
            return message
        except Exception as err:
            raise err

    def bulk_modify(self, body):
        """
        Modify the mails in the user's mailbox
        :Args:
            body = payload with the ids of the labels that are going to modify
        Returns:
            List of modified mails, containing updated labelIds, id and threadId.
        """

        try:
            messages = self.service.users().messages().batchModify(userId=self.user_id, body=body).execute()
            return messages
        except Exception as err:
            raise err

    def mark_as_read(self, message_id):
        """Mark an email as read in user's inbox"""

        msg_labels = {
            "removeLabelIds": [
                "UNREAD"
            ]
        }
        return self.modify(message_id, msg_labels)

    def bulk_mark_as_read(self, message_ids):
        """Marks multiple emails as read in user's inbox"""

        msg_labels = {
            "ids": message_ids,
            "removeLabelIds": [
                "UNREAD"
            ]
        }
        return self.bulk_modify(msg_labels)

    def move(self, message_id, label):
        """Move email to labels like INBOX, SPAM, etc"""

        msg_labels = {
            "addLabelIds": [
                label
            ]
        }
        return self.modify(message_id, msg_labels)

    def bulk_move(self, message_ids, label_ids):
        """Move multiple emails to labels like INBOX, SPAM, etc"""

        msg_labels = {
            "ids": message_ids,
            "addLabelIds": label_ids
        }
        return self.bulk_modify(msg_labels)


def main():
    pass


if __name__ == '__main__':
    main()
