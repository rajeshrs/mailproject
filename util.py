import re
import base64
import email
import pytz
import json
from datetime import datetime
from bs4 import BeautifulSoup


def parse_header(headers):
    """Parse the value of mail_from, mail_subject and received_time from the mail headers"""

    mail_subject, mail_from, received_time = None, None, None
    for header in headers:
        if header['name'] == 'Subject':
            mail_subject = header["value"]
        if header['name'] == 'From':
            mail_from = header['value']
            mail_from_string = re.findall('<(.*?)>', mail_from)
            if len(mail_from_string) > 0:
                mail_from = mail_from_string[0]
        if header['name'] == 'Date':
            received_time = header['value']
            received_time = set_datetime_format(received_time)

    return mail_subject, mail_from, received_time


def parse_body(payload):
    """Parse the body text from the mail data"""

    body_text = ''
    if 'parts' in payload:
        parts = payload.get('parts')
        for part in parts:
            if part['mimeType'] == 'text/plain':
                text_body = part['body']['data']
                msg_str = base64.urlsafe_b64decode(text_body.encode('ASCII'))
                body_text = email.message_from_bytes(msg_str)
                body_text = body_text.as_string()
    else:
        text_body = payload['body']['data']
        encoded_body_text = base64.urlsafe_b64decode(text_body.encode('ASCII'))
        if payload['mimeType'] == 'text/plain':
            body_text = email.message_from_bytes(encoded_body_text)
            body_text = body_text.as_string()

        elif payload['mimeType'] == 'text/html':
            soup = BeautifulSoup(encoded_body_text, features='html.parser')
            for script in soup(['script', 'style']):
                script.extract()
            body_text = soup.get_text()

    return body_text


def set_datetime_format(given_time):
    """Set the right datetime format for received date before inserting into mysql db"""

    given_time = given_time.split(', ')[1]
    tz_string_data = re.findall('\((.*?)\)', given_time)
    if len(tz_string_data) > 0:
        given_time = given_time.replace(' ({})'.format(tz_string_data[0]), '')

    if '+' in given_time or '-' in given_time:
        given_time = datetime.strptime(given_time, "%d %b %Y %H:%M:%S %z")
    else:
        given_time = datetime.strptime(given_time, "%d %b %Y %H:%M:%S")
    tz = pytz.timezone('Asia/Kolkata')
    return given_time.astimezone(tz)


def get_rules():
    """Get the rules from the json file rules.json"""

    with open('rules.json', 'rb') as file:
        rules = json.load(file)
    return rules
