from db import DB
from util import get_rules
from gmail_api import GmailAPI

gmail_api = GmailAPI()
db = DB()


def process_mails():
    """Process the mails and do actions based on the rules in the file rules.json"""

    rules = get_rules()
    base_query = "SELECT id FROM mails WHERE {};"
    for rule in rules:
        operation = " AND " if rule['predicate'] == "all" else " OR "
        final_query = base_query.format(process_conditions(rule['conditions'], operation))
        mail_ids = db.filter(final_query)
        if len(mail_ids) > 0:
            for action in rule['actions']:
                run_action(action, mail_ids)


def process_conditions(conditions, operation):
    """Check the conditions of the rules and prepare the mysql query with those conditions"""

    query = ""
    query_options = {
        "contains": "{} LIKE '%{}%'{}",
        "not_contains": "{} NOT LIKE '%{}%'{}",
        "equals": "{} = '{}'{}",
        "not_equals": "{} != '{}'{}",
        "less_than": "{} < '{}'{}",
        "greater_than": "{} > '{}'{}"
    }

    total_conditions = len(conditions)
    for i in range(total_conditions):
        condition = conditions[i]
        operation = '' if i == total_conditions - 1 else operation
        query += query_options[condition['predicate']].format(condition['field'], condition['value'], operation)

    return query


def run_action(action, mail_ids):
    """Do the action on mails filtered by rule conditions in user's inbox"""
    label_type = action['type']
    if label_type == 'mark_as_read':
        gmail_api.bulk_mark_as_read(mail_ids)
    elif label_type == 'move':
        all_labels = gmail_api.list_labels()
        all_label_ids = [label['id'] for label in all_labels]
        if action['to_label'] in all_label_ids:
            label_ids = [action['to_label'] for mail_id in mail_ids]
            return gmail_api.bulk_move(mail_ids, label_ids)


def main():
    process_mails()


if __name__ == '__main__':
    main()
