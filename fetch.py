from db import DB
from gmail_api import GmailAPI

gmail_api = GmailAPI()
db = DB()


def fetch_and_store_mails():
    """
    Fetches the mails from Gmail inbox and store it in mysql db
    """
    mails = gmail_api.fetch_mails()
    db.bulk_insert(mails)
    db.close()


def main():
    fetch_and_store_mails()


if __name__ == '__main__':
    main()
