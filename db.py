import mysql.connector
from mysql.connector import errorcode
from __init__ import get_config


class DB(object):

    def __init__(self):
        self.section = 'mysql'
        self.config = get_config(self.section)
        self.host = self.config.get('HOSTNAME')
        self.user = self.config.get('USERNAME')
        self.password = self.config.get('PASSWORD')
        self.db_name = self.config.get('DBNAME')
        self.conn = self.connect()
        self.cursor = self.conn.cursor()
        self.use_or_create_database()

    def connect(self):
        """Connect to the database"""
        try:
            return mysql.connector.connect(host=self.host, user=self.user, password=self.password)
        except mysql.connector.Error as err:
            raise err

    def filter(self, query):
        """ Filter the mails base on the query in the db table `mails`"""
        try:
            self.cursor.execute(query)
            records = self.cursor.fetchall()
            mail_ids = [ record[0] for record in records]
            return mail_ids
        except mysql.connector.Error as err:
            raise err

    def list_mails(self):
        """List the mails from the db table `mails`"""
        try:
            self.cursor.execute("SELECT id, mail_from, mail_subject, message, label_ids, received_time FROM mails;")
            records = self.cursor.fetchall()
            mails = []
            for record in records:
                mail = {
                    'id': record[0],
                    'mail_from': record[1],
                    'mail_subject': record[2],
                    'message': record[3],
                    'received_time': record[4]
                }
                mails.append(mail)
            return mails
        except mysql.connector.Error as err:
            raise err

    def use_or_create_database(self):
        """Use if the db name exists or create one"""
        try:
            return self.cursor.execute("USE {}".format(self.db_name))
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                self.cursor.execute("CREATE DATABASE {};".format(self.db_name))
                self.conn.database = self.db_name
            else:
                raise err

    def create_table(self, query):
        """Create a table in the db"""
        try:
            self.cursor.execute(query)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                pass

    def insert_data(self, data):
        """Insert a new record into the table"""
        query = self.table_insertion_query()
        try:
            self.cursor.execute(query, data)
            self.conn.commit()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_NO_SUCH_TABLE:
                table_description = self.table_creation_query()
                self.create_table(table_description)
                self.cursor.execute(query, data)
            else:
                return err

    def bulk_insert(self, bulk_data):
        """Insert multiple records into the table"""
        query = self.table_insertion_query()
        try:
            self.cursor.executemany(query, bulk_data)
            self.conn.commit()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_NO_SUCH_TABLE:
                table_description = self.table_creation_query()
                self.create_table(table_description)
                self.cursor.executemany(query, bulk_data)
                self.conn.commit()
            else:
                raise err

    @staticmethod
    def table_creation_query():
        """Query to create a table `mails` in the db"""
        return ("CREATE TABLE `mails` ("
                "  `id` VARCHAR(255) NOT NULL PRIMARY KEY,"
                "  `mail_from` VARCHAR(255) NOT NULL,"
                "  `mail_subject` VARCHAR(255),"
                "  `message` TEXT,"
                "  `label_ids` VARCHAR(255),"
                "  `received_time` DATETIME"
                ") ENGINE=InnoDB")

    @staticmethod
    def table_insertion_query():
        """Query to insert the data into the table `mails` in the db"""
        return ("INSERT IGNORE INTO mails "
                "(id, mail_from, mail_subject, message, label_ids, received_time) "
                "VALUES (%(id)s, %(mail_from)s, %(mail_subject)s, %(message)s, %(label_ids)s, %(received_time)s)")

    def close(self):
        """Close the db connection"""
        self.cursor.close()
        self.conn.close()
